## Vue components

Two commonly used components:

- SpinBox: input with up and down arrow buttons on the side to increment value in input
- DropDown: input or display with button on the side that reveals list of elements
