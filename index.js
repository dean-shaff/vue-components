import Vue from "vue"

import SpinBox from "./SpinBox/SpinBox.vue"
import InputSpinBox from "./SpinBox/InputSpinBox.vue"
import DropDown from "./DropDown/DropDown.vue"
import InputDropDown from "./DropDown/InputDropDown.vue"

const vue = new Vue({
    components:{
        "spin-box":SpinBox,
        "input-spin-box":InputSpinBox,
        "drop-down":DropDown,
        "input-drop-down":InputDropDown
    },
    methods:{
        onSpinBoxValue: function(val){
            console.log(val)
        },
        onInputSpinBoxValue: function(val){
            console.log(val)
        },
        onInputSpinBoxAccept: function(val){
            console.log(`accept: ${val}`)
        },
        onDropDownClick: function(val){
            console.log(val)
        },
        onInputDropDownClick: function(val){
            console.log(val)
        }
    },
    data:function(){
        return {
            contents: ["Element1", "Element2"]
        }
    },
    el:"#main",
    template:`<div>
    <spin-box @spin-box-value="onSpinBoxValue"></spin-box>
    <input-spin-box @spin-box-value="onInputSpinBoxValue" @input-spin-box-accept="onInputSpinBoxAccept"></input-spin-box>
    <div class="row">
        <div class="three columns">
        <drop-down :contents="contents" @drop-down-click="onDropDownClick"></drop-down>
        </div>
        <div class="three columns">
        <input-drop-down :contents="contents" @drop-down-click="onInputDropDownClick"></input-drop-down>
        </div>
    </div>
    </div>`
})
